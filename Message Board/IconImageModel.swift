//
//  IconImageModel.swift
//  Message Board
//
//  Created by Abdul Mojid on 04/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class ImageFiles {
    
    let ImageManager = FileManager.default
    
    static let shared = ImageFiles()
    
    func save(_ image: UIImage, imageName: String) {
        
        let paths = ImageManager.urls(for: .cachesDirectory, in: .userDomainMask)
        
        if var location = paths.first {
            
            location.appendPathComponent(imageName)
            
            if let imageData = image.pngData() {
                
                ImageManager.createFile(atPath: location.path, contents: imageData, attributes: nil)
            }
        }
    }
    
    func get(_ imageNamed: String, namedFolder: String) -> UIImage? {
        
        let paths = ImageManager.urls(for: .cachesDirectory, in: .userDomainMask)
        
        if var location = paths.first {
            
            location.appendPathComponent(namedFolder)
            
            location.appendPathComponent(imageNamed)
            
           
            if ImageManager.fileExists(atPath: location.path) {
                
                do {
                    let someData = try Data(contentsOf: location)
                    
                    let image = UIImage(data: someData)
                    return image
                } catch {
                    print(error)
                    return nil
                }
            }
        }
        return nil
    }
}
