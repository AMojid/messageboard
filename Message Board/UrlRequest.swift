//
//  UrlRequest.swift
//  Message Board
//
//  Created by Abdul Mojid on 03/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation
import UIKit

enum Endpoints {
    static let users = "/users"
    static let posts = "/posts"
    static let comments = "/comments"
    static let avatars = "/avarters/285/"
}

enum Typicode {
    static let scheme = "http"
    static let host = "jsonplaceholder.typicode.com"
}

enum Images {
    static let scheme = "http"
    static let host = "avarters.adorable.io"
}




class DataFetcher {
    
    let session: URLSession
    
//    init(_ session: URLSession ) {
//        self.session = session
//    }
    
    init(_ sessionConfiguration: URLSessionConfiguration = .ephemeral) {
        session = URLSession(configuration: sessionConfiguration)
    }

    static let shared = DataFetcher()
    
//    enum ReponseError: Error {
//        case not200
//        case unknown
//    }
//
//     private func fetchData(from url: URL, completion: @escaping (Result<Data>) -> Void) {
//
//        let request = URLRequest(url: url)
//
//        let dataTask = session.dataTask(with: request) { (data, response, error) in
//
//            if let error = error {
//                completion(.failure(error))
//            } else if let data = data, let response = response as? HTTPURLResponse {
//
//                switch response.statusCode {
//                case 200...299: completion(.success(data))
//                default: completion(.failure(ReponseError.not200))
//                }
//
//            } else {
//                completion(.failure(ReponseError.unknown))
//            }
//        }
//        dataTask.resume()
//    }
    
    
    // A private function to fetch data from a URL
    private func fetchData(from url: URL, completion: @escaping (Data?) -> Void) {
        
        let request = URLRequest(url: url)
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error)
                completion(nil)
            } else if let data = data {
                completion(data)
            }
        }
        
        dataTask.resume()
        
    }
    
    
}

extension DataFetcher {
    func fetchUsersDetails(url: URL,  completion: @escaping ([UserRoot]?) -> Void) {
        
        fetchData(from: url) { data in
            
            if let data = data {
          
                do {
                    let value = try JSONDecoder().decode([UserRoot].self, from: data)
                    completion(value)
                } catch {
                    print(error)
                    completion(nil)
                }
                
            } else {
                completion(nil)
            }
        }
    }    
}

extension DataFetcher {
    
    func fetchUserPosts(url: URL,  completion: @escaping ([UserPost]?) -> Void) {
        
        fetchData(from: url) { data in
            
            if let data = data {
    
                guard let dataString = String(data: data, encoding: .utf8) else { return completion(nil) }
                
                let safeData = dataString.replacingOccurrences(of: "\n", with: "")
                guard let fixedData = safeData.data(using: .utf8) else { return completion(nil) }
                
                do {
                    let value = try JSONDecoder().decode([UserPost].self, from: fixedData)
                    completion(value)
                } catch {
                    print(error)
                    completion(nil)
                }
                
            } else {
                completion(nil)
            }
        }
    }
}

extension DataFetcher {
    
    func fetchImage(imageURL: URL, completion: @escaping (UIImage?) -> Void) {
        
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            guard let self = self else {
                return
            }
            self.fetchData(from: imageURL) { data in
                if let data = data {
                    completion(UIImage(data: data))
                } else {
                    completion(nil)
                }
            }
        }
    }
}
