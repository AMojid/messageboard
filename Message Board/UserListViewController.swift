//
//  ViewController.swift
//  Message Board
//
//  Created by Abdul Mojid on 01/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
// text setpu

import UIKit

protocol DataReloadTableViewDelegate: class{
    func reloadNetworkTable()
}

class UserListViewController: UIViewController {
    
    
    var reuserID = "UITableViewCell"
    var model: UserModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        model = UserModel()
        model.delegate = self
        fetchUser()
        
    }



@objc func fetchUser() {
       
    var urlComponents = URLComponents()
           
           urlComponents.scheme = Typicode.scheme
           urlComponents.host = Typicode.host
           urlComponents.path = Endpoints.users
           
           guard let url:URL = urlComponents.url else { return }
           
    DataFetcher.shared.fetchUsersDetails(url: url, completion: { value in
               
               guard let users:[UserRoot] = value else { return }
        self.model.saveUser(arrayUsers: users)
           })
       
   }

}

    
//MARK: - Table view data source & Delegate 
extension UserListViewController: UITableViewDelegate, UITableViewDataSource {
   
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.totalUser
        }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: reuserID, for: indexPath)
            cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = model.users[indexPath.row].name
            return cell
        }
    
}


class SubtitleTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not found")
    }
}

extension UserListViewController: DataReloadTableViewDelegate{
    
    func reloadNetworkTable(){
        DispatchQueue.main.async {
            
           
            //self.tableView.reloadData()
        }
    }
    
}


