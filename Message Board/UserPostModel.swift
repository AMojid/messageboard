//
//  UserPostModel.swift
//  Message Board
//
//  Created by Abdul Mojid on 04/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation

struct UserPost: Codable {
    let body: String
    let id: Int
    let post_title: String
    let userId: Int

    private enum CodingKeys: String, CodingKey {
        case body
        case id
        case post_title = "title"
        case userId
    }
}

class ModelPost {
    
    var userPosts:[UserPost]
    
    init(){
        
        userPosts = []
    }
    
}
