//
//  UserDetailsViewController.swift
//  Message Board
//
//  Created by Abdul Mojid on 03/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import UIKit

class UserDetailsViewController: UIViewController {
    
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var emailLabel: UILabel!
    
    @IBOutlet var addressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
