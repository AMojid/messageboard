//
//  UserModel.swift
//  Message Board
//
//  Created by Abdul Mojid on 03/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation

struct UserRoot: Codable {
    let id: Int
    let name: String
    let username: String
    let email: String
    let address: UserAddress
    let phone: String
    let website: String
//    let company: UserCompany
}

struct UserAddress: Codable {
    let street: String
    let suite: String
    let city: String
    let zipcode: String
}
//
//struct UserCompany: Codable {
//    let companyName: String
//    let catchPrase: String
//    let bs: String
//
//    enum CodingKeys: String, CodingKey {
//        case companyName = "name"
//        case catchPrase
//        case bs
//    }
//}
    class UserModel   {
        
        weak var delegate: DataReloadTableViewDelegate?
        
        var users: [UserRoot]
        var totalUser:Int {
            get {return users.count}
        }
        
        init(){
            users = []
        }
    
    
    func saveUser(arrayUsers: [UserRoot]){
        
        users = arrayUsers
        getImage()
        delegate?.reloadNetworkTable()
    }
    func getImage(){
        
        var urlComponents = URLComponents()
        
        for user in users{
            
            urlComponents.scheme = Images.scheme
            urlComponents.host = Images.host
            urlComponents.path = Endpoints.avatars + user.name
            
            guard let url:URL = urlComponents.url else { return }
            
            DataFetcher.shared.fetchImage(imageURL: url, completion: { image in
                
                guard let image = image else { return }
                
                ImageFiles.shared.save(image, imageName: user.name)
            })
        }
    }

    
    }
