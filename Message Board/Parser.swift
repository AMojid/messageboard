//
//  Parser.swift
//  Message Board
//
//  Created by Abdul Mojid on 03/11/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation
enum Result<T> {
    case success(T)
    case failure(Error)
}

protocol Parser {
    func parse<T>(_ data: Data, into type: T.Type) throws -> T where T: Codable
}

struct JSONParser: Parser {
    
    func parse<T>(_ data: Data, into type: T.Type) throws -> T where T : Decodable {
        return try JSONDecoder().decode(type, from: data)
    }

}
